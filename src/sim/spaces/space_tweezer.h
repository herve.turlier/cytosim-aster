// Cytosim 3.0
//
//  File created by Hervé Turlier on 2/13/19.
//  Copyright © 2019 Collège-de-France. All rights reserved.

#ifndef space_tweezer_h
#define space_tweezer_h

#include "dim.h"
#include "space_sphere.h"

/// An harmonic trap under sinusoidal movement to simulate an (optical) tweezer
/**
 Space `tweezer` is a point-like space (zero volume), centered around the origin, under sinusoidal movement of given amplitude in x-direction
 An object confined in this spaces will feel a linear restoring force characterized by the 'stiffness' of the interaction.
 
 - amplitude = amplitude of the oscillatory movement of the trap in x-direction
 - frequency = driving frequency of the oscillatory trap movement

 @ingroup SpaceGroup
 Forces registered with 'setInteractions' are added, and used to create the trap force.
 The harmonic trap stiffness is set directly in the interaction.
 The trap force on an object can be calculated a posteriori from the interaction stiffness k, trap position x_trap and trapped object center of mass x_CM as
 @code
 F = k (x_CM - x_trap)
 @endcode

 To extract the trap position over time, the report function can be used with argument space:tweezer.
 @ingroup SpaceGroup
 
 
 Created by Hervé Turlier, Paris 13.02.2019
 Modified on 03.03.2019
 */

class SpaceTweezer : public Space
{

private:
    
    /// trap center
    Vector      center_;

    /// frequency of trap harmonic driving
    real        frequency_;
    
    /// the amplitude of trap driving
    real        amplitude_;
    //real        amplitude() const { return Space::length(0); }
    
    /// simulation time
    real        time;

public:
    /// constructor
    SpaceTweezer(const SpaceProp*);
    
    /// check number and validity of specified lengths
    // void        resize() { Space::checkLengths(1, true); }
    void        resize(Glossary& opt);

    /// no update implemented at this point
    void  update();
    
    // maximum extension along each axis
    //Vector      extension() const;
    
    /// return bounding box in `inf` and `sup`
    void        boundaries(Vector& inf, Vector& sup) const;
    
    /// the volume inside (zero)
    real        volume() const;
    
    /// true if the point is inside the Space - always true here to have an interaction
    //bool        inside(const real point[]) const;
    bool        inside(Vector const&) const;

    /// true if the point is inside the Space - always true here to have an interaction
    //bool        allInside(const real point[], real rad) const;
    bool        allInside(Vector const&, real rad) const;

    /// always true here to have an interaction
    //bool        allOutside(const real point[], real rad) const;
    bool        allOutside(Vector const&, real rad) const;

    /// set `proj` as the point on the edge that is closest to `point`
    Vector      project(Vector const& pos) const;

    /// apply an harmonic force directed towards the center of the trap
    //void        setInteraction(Vector const& pos, PointExact const&, Meca &, real stiff) const;
    void        setInteraction(Vector const& pos, Mecapoint const&, Meca &, real stiff) const;

    /// apply an harmonic force directed towards the center of the trap
    //void        setInteraction(Vector const& pos, PointExact const&, real rad, Meca &, real stiff) const;
    void        setInteraction(Vector const& pos, Mecapoint const&, real rad, Meca &, real stiff) const;

    /// the step function will move the trap center
    void        step();
    
    /// read from file
    //virtual void  read(InputWrapper& , Simul&);
    void        read(Inputter&, Simul&, ObjectTag);

    /// write to file
    //virtual void  write(OutputWrapper&) const;
    void        write(Outputter&) const;

    /// get dimensions from array 'len'
    void        setLengths(const real len[8]);
    
    /// return tweezer center
    void report(std::ostream&) const;
    
    /// OpenGL display function, return true is display was done
    //bool        display() const;
    bool        draw() const;

};
#endif /* space_tweezer_h */
