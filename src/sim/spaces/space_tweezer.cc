// Cytosim 3.0
//
//  File created by Hervé Turlier on 2/13/19.
//  Modified on 1/14/22
//  Copyright © 2019 Collège-de-France. All rights reserved.

#include "space_tweezer.h"
#include "exceptions.h"
#include "smath.h"
#include "simul.h"
#include <math.h>
#include "meca.h"
#include "iowrapper.h"


SpaceTweezer::SpaceTweezer(const SpaceProp* p) :
Space(p)
{
    amplitude_ = 0;
    center_.reset();
    frequency_ = 0;
    time = 0;
}

//------------------------------------------------------------------------------
//                         Geometrical redefinitions
//------------------------------------------------------------------------------

void SpaceTweezer::resize(Glossary& opt)
{
    real amp = amplitude_;
    real freq = frequency_;
    Vector cent = center_;
    
//    for ( int d = 0; d < DIM; ++d )
//        opt.set(center_[d], "center", d);

    opt.set(cent, "center");
    center_ = cent;

    opt.set(amp, "amplitude");
    if ( amp < 0 )
        throw InvalidParameter(prop->name()+"amplitude should be >= 0");
    amplitude_ = amp;

    opt.set(freq, "frequency");
    if ( freq < 0 )
        throw InvalidParameter(prop->name()+"frequency should be >= 0");
    frequency_ = freq;
        
    update();
}

void SpaceTweezer::update()
{
    // nothing to do
}

void SpaceTweezer::boundaries(Vector& inf, Vector& sup) const
{
    inf.set(-amplitude_, 0, 0);
    sup.set(amplitude_, 0, 0);
}


real SpaceTweezer::volume() const
{
    return 0;
}

// return false to always have an interaction
bool SpaceTweezer::inside(Vector const& pos) const
{
    return false;
}

// return false to always have an interaction
bool  SpaceTweezer::allInside( Vector const&, real rad ) const
{
    return false;
}

// return true to always have an interaction
bool  SpaceTweezer::allOutside( Vector const&, real rad ) const
{
    return true;
}

Vector  SpaceTweezer::project(Vector const& pos) const
{
    return center_;
}

//------------------------------------------------------------------------------
//                         Interaction -> trap force
//------------------------------------------------------------------------------


void SpaceTweezer::setInteraction(Vector const& pos, Mecapoint const& pe, Meca & meca, real stiff) const
{
    meca.addPointClamp(pe, center_, stiff);
}


void SpaceTweezer::setInteraction(Vector const& pos, Mecapoint const& pe, real rad, Meca & meca, real stiff) const
{
    meca.addPointClamp(pe, center_, stiff);
}


//------------------------------------------------------------------------------
//                                  IO
//------------------------------------------------------------------------------

void  SpaceTweezer::write(Outputter& out) const
{
    out.put_characters("tweezer", 16);
    out.writeUInt16(2+DIM);
    out.writeFloat(amplitude_);
    out.writeFloat(frequency_);
    for ( int d = 0; d < DIM ; ++d )
        out.writeDouble(center_[d]);
}

void SpaceTweezer::setLengths(const real len[])
{
    amplitude_ = len[0];
    frequency_ = len[1];
    for (int d = 0; d < DIM ; ++d)
        center_[d] = len[2+d];
    update();
}

void  SpaceTweezer::read(Inputter& in, Simul& sim, ObjectTag)
{
    real len[8] = { 0 };
    read_data(in, len, "tweezer");
    setLengths(len);
}

void SpaceTweezer::report(std::ostream& out) const
{
    out << "% tweezer position" << '\n';
    out << center_ ;
    out << '\n';
    out << "% tweezer frequency" << '\n';
    out << frequency_ ;
    out << '\n';
    out << "% tweezer amplitude" << '\n';
    out << amplitude_ ;
    out << '\n';
}

//------------------------------------------------------------------------------
//                      Dynamic space time-stepping
//------------------------------------------------------------------------------


void SpaceTweezer::step()
{
    // updating time
    time += prop->time_step;
    // updating trap position
    center_.XX = amplitude_*cos(2*M_PI*frequency_*time + M_PI/2);
}


//------------------------------------------------------------------------------
//                         OPENGL  DISPLAY
//------------------------------------------------------------------------------

#ifdef DISPLAY
#include "opengl.h"
#include "gle.h"


bool SpaceTweezer::draw() const
{
#if ( DIM > 2 )

    const size_t fin = 512;

    GLfloat L = (GLfloat)amplitude_;
    GLfloat R = (GLfloat)0.1;
    
    GLfloat c[fin+1], s[fin+1];
    gle::circle(fin, c, s, 1);
    
    glBegin(GL_TRIANGLE_STRIP);
    for ( size_t sc = 0; sc <= fin; ++sc )
    {
        GLfloat ca = c[sc], sa = s[sc];
        glNormal3f( 0, ca, sa );
        glVertex3f( +L, R*ca, R*sa );
        glVertex3f( -L, R*ca, R*sa );
    }
    glEnd();
    
    // draw the cap:
    glBegin(GL_TRIANGLE_FAN);
    glNormal3f( +1, 0, 0 );
    glVertex3f( +L, 0, 0 );
    for ( size_t sc = 0; sc <= fin; ++sc )
        glVertex3f( +L, R*c[sc], R*s[sc] );
    glEnd();
    
    // draw the cap:
    glBegin(GL_TRIANGLE_FAN);
    glNormal3f( -1, 0, 0 );
    glVertex3f( -L, 0, 0 );
    for ( size_t sc = 0; sc <= fin; ++sc )
        glVertex3f( -L,-R*c[sc], R*s[sc] );
    glEnd();
    
#endif
    return true;
}

#else

bool SpaceTweezer::draw() const
{
    return false;
}

#endif
